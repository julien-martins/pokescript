from selenium import webdriver
from selenium.webdriver.common.by import By

from DungeonTile import DungeonTile, DungeonTileType

class Dungeon:
    def __init__(self, dungeon_board_dom) -> None:
        self.root = dungeon_board_dom
        self.width = len(dungeon_board_dom.find_elements(By.TAG_NAME, "tr"))
        self.height = len(dungeon_board_dom.find_elements(By.TAG_NAME, "tr")[0].find_elements(By.TAG_NAME, "td"))
        self.tiles = []
        self.player_x = 0
        self.player_y = 0
        self.fillBoard()

    def fillBoard(self):
        tds = self.root.find_elements(By.CLASS_NAME, "tile")
        for i in range(self.width * self.height):
            attribute = tds[i].get_attribute("class")
            state = attribute.split('-')[1]
            if state == "player":
                self.player_x = i % self.height
                self.player_y = (int)(i / self.height)
            dungeon_tile = DungeonTile(i, tds[i], state, self.width, self.height)
            self.tiles.append(dungeon_tile)

    def DrawingBoard(self):
        for i in range(self.width * self.height):
            text = "{: ^10} | ".format(self.tiles[i].state)
            if i % self.height == 4:
                print(text[:-2], end="")
                print("\n")
            else:
                print(text, end="")
    
    def getPlayerPosition(self):
        return (self.player_x, self.player_y)

    def Explore(self, driver):
        boss_finded = False

        not_visited_tiles = []
        while not boss_finded:
            # different direction to test around the player
            # cross form
            dirs = [(1, 0), (-1, 0), (0, 1), (0, -1)]
            
            for dir in dirs:
                new_player_x = self.player_x + dir[0]
                new_player_y = self.player_y + dir[1]

                # Test if the new position is out of bounds
                if not self.InBounds(new_player_x, new_player_y):
                    continue
                
                index = new_player_y * self.height + new_player_x
                
                if self.tiles[index].visited == True :
                    continue

                not_visited_tiles.append(self.tiles[index])

            if len(not_visited_tiles) <= 0 : break
            tile = not_visited_tiles.pop()

            # Move the player at the new position
            self.MovePlayerAt(tile)

            # In function at what there are in the screen
            # we defined the state of the tile
            # Chest => DungeonTileType.Chest
            # progress-bar btn-danger => DungeonTileType.EMPTY
            # progress-bar boss-button => DungeonTileType.BOSS

            # Wait that the screen has no pokemon
            # //div[@id="battleContainer"]//div[contains(@class, "progress-bar")]
            # progress-bar bg-danger => Pokemon
            # progress-bar healthbar-boss => Boss

            if self.IsBoss(driver):
                driver.find_element(By.XPATH, '//button[contains(@class,"dungeon-button")]').click()
                self.WaitEnemy(driver, "healthbar-boss")
                print("BOSS BEATEEN")
                boss_finded = True
            else:
                self.WaitEnemy(driver, "bg-danger")
            
            # self.DrawingBoard()

    def InBounds(self, x, y):
        return not (x >= self.width
                or y >= self.height
                or x < 0
                or y < 0)

    def MovePlayerAt(self, tile):
        tile.Click()
        self.tiles[tile.index].visited = True
        self.player_x = tile.x
        self.player_y = tile.y
    
    def IsBoss(self, driver):
        cond = driver.find_elements(By.XPATH, '//button[contains(@class,"dungeon-button")]')
        return len(cond) > 0

    # parameter 
    # if boss => healthbar-boss
    # if pokemon => bg-danger
    def WaitEnemy(self, driver, parameter):
        cond = driver.find_elements(By.XPATH, '//div[@id="battleContainer"]//div[contains(@class, "progress-bar '+ parameter +'")]')
        
        while(len(cond) >= 1):
            cond = driver.find_elements(By.XPATH, '//div[@id="battleContainer"]//div[contains(@class, "progress-bar '+ parameter +'")]')   
