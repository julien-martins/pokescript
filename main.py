import pathlib
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By

from Dungeon import Dungeon
from DungeonTile import *

dungeons = [
    "Cerulean Cave",
    "Pokemon Tower"
]

url = 'http://www.pokeclicker.com'
options = webdriver.ChromeOptions()

scriptDirectory = pathlib.Path().absolute()

options.add_argument(f"user-data-dir={scriptDirectory}\\selenium")
driver = webdriver.Chrome(options=options)
driver.get(url)

# Click on save
trainer_card = driver.find_element(by=By.CLASS_NAME, value='trainer-card')
trainer_card.click()

# Check if in loading screen
loading_screen = driver.find_element(By.CLASS_NAME, 'loader').value_of_css_property('display')
print("LOADING SCREEN STATUS")

while loading_screen == "block":
    loading_screen = driver.find_element(By.CLASS_NAME, 'loader').value_of_css_property('display')

print("TRY TO FIND DUNGEON")
# Dungeon
dungeon_to_farm = "Pokemon Tower"
dungeon = driver.find_element(By.XPATH, '//*[name()="svg"][@id="map"]/*[name()="g"]/*[name()="rect"][@data-town="' + dungeon_to_farm + '"]')

dungeon.click()

explore_count = 20
for i in range(explore_count):
    print(f"ENTER IN THE DUNGEON x {i}")
    start_dungeon_button = driver.find_element(By.XPATH, "//*[@id='townView']//button[contains(@class, 'btn-success')]")
    start_dungeon_button.click()

    dungeon_board = driver.find_element(By.CLASS_NAME, "dungeon-board")
    dungeon = Dungeon(dungeon_board)
    dungeon.Explore(driver)

driver.quit()