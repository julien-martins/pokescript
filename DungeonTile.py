from enum import Enum
from selenium.webdriver.common.by import By

class DungeonTileType(Enum):
    PLAYER = 1
    INVISIBLE = 2
    ENTRANCE = 3
    EMPTY = 4
    CHEST = 5
    BOSS = 6

class DungeonTile:
    def __init__(self):
         # player | invisible | entrance | empty | chest | boss
        self.index = 0
        self.state = DungeonTileType.EMPTY
        self.elt = None
        self.width = 0
        self.height = 0
        self.x = 0
        self.y = 0
        self.visited = False
    
    def __init__(self, index, elt, state, width, height):
        self.index = index
        self.state = state
        self.elt = elt
        self.width = width
        self.height = height
        self.x = index % self.height
        self.y = (int)(index / self.height)
        if state == DungeonTileType.PLAYER :
            self.visited = True
        else :
            self.visited = False
    
    def Click(self):
        self.elt.click()
        
    def setVisited(self, value):
        self.visited = value

    def setElement(self, elt):
        self.elt = elt

    def setState(self, state):
        self.state = state

    def setSize(self, width, height):
        self.width = width
        self.height = height